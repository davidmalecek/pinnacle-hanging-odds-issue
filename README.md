# Simple Pinnacle Odds Parser

Parses fetched odds from Pinnacle - retrieved from Cloudwatch.

The raw log is available in [data/cloudwatch-20190304-20190305-filter-959723927.log](data/cloudwatch-20190304-20190305-filter-959723927.log).
The splitted `xx*.json` files are available in [data/](data/). These files are input for the [parse-pinnacle-odds.groovy](parse-pinnacle-odds.groovy).

The log was splitted using following commands:
```bash
csplit cloudwatch-20190304-20190305-filter-959723927.log '/2019-03-0.*DEBUG.*Fetched data:/' {1000}
```
```bash
for i in xx*; do `sed '/2019-03-0.*DEBUG.*Fetched data:/d' $i > $i.json`; done
```

By running the script
```bash
groovy parse-pinnacle-odds.groovy 959723927
```

you will get following output (`lineId` => `period`):

```
672260311 => [moneyline:[home:3.59, away:1.263], spreads:null, status:1]
672259769 => [moneyline:[home:2.29, away:1.578], spreads:null, status:1]
672258908 => [moneyline:[home:3.17, away:1.325], spreads:null, status:1]
672258626 => [moneyline:[home:3.37, away:1.293], spreads:null, status:1]
672259838 => [moneyline:[home:2.48, away:1.5], spreads:null, status:1]
672256290 => [moneyline:[home:3.22, away:1.317], spreads:[[hdp:1.5, home:1.769, away:1.98]], status:2]
672259389 => [moneyline:[home:2.65, away:1.444], spreads:null, status:1]
672259317 => [moneyline:[home:2.38, away:1.54], spreads:null, status:1]
672256653 => [moneyline:[home:2.64, away:1.448], spreads:null, status:1]
672260264 => [moneyline:[home:3.13, away:1.333], spreads:null, status:1]
672260444 => [moneyline:[home:3.24, away:1.313], spreads:null, status:1]
672260452 => [moneyline:[home:3.59, away:1.263], spreads:null, status:1]
672260278 => [moneyline:[home:3.89, away:1.23], spreads:null, status:1]
672257816 => [moneyline:[home:2.85, away:1.392], spreads:null, status:1]
672259277 => [moneyline:null, spreads:null, status:1]
672259278 => [moneyline:[home:2.2, away:1.625], spreads:null, status:1]
672259364 => [moneyline:[home:2.82, away:1.4], spreads:null, status:1]
672256329 => [moneyline:[home:3.22, away:1.317], spreads:[[hdp:1.5, home:1.769, away:1.98]], status:1]
672259831 => [moneyline:[home:2.46, away:1.51], spreads:null, status:1]
672258665 => [moneyline:[home:3.35, away:1.296], spreads:null, status:1]
672258971 => [moneyline:[home:3.07, away:1.344], spreads:null, status:1]
672260367 => [moneyline:[home:3.98, away:1.222], spreads:null, status:1]
672259679 => [moneyline:[home:2.23, away:1.606], spreads:null, status:1]
672259483 => [moneyline:[home:2.32, away:1.568], spreads:null, status:1]
672259194 => [moneyline:[home:2.44, away:1.515], spreads:null, status:1]
672258204 => [moneyline:[home:2.64, away:1.446], spreads:null, status:1]
672260224 => [moneyline:[home:3.08, away:1.341], spreads:null, status:1]
672260494 => [moneyline:[home:3.71, away:1.25], spreads:null, status:1]
672259582 => [moneyline:[home:2.14, away:1.657], spreads:null, status:1]
672259101 => [moneyline:null, spreads:null, status:2]
672259103 => [moneyline:[home:3.07, away:1.344], spreads:null, status:2]
672258342 => [moneyline:[home:2.68, away:1.434], spreads:null, status:1]
672260055 => [moneyline:null, spreads:null, status:2]
672260056 => [moneyline:[home:2.41, away:1.526], spreads:null, status:2]
672260584 => [moneyline:null, spreads:null, status:2]
672260585 => [moneyline:[home:3.24, away:1.313], spreads:null, status:2]
672258456 => [moneyline:[home:2.57, away:1.469], spreads:null, status:1]
672259133 => [moneyline:[home:3.07, away:1.344], spreads:null, status:2]
672259559 => [moneyline:[home:2.04, away:1.724], spreads:null, status:1]
672260499 => [moneyline:[home:3.48, away:1.277], spreads:null, status:1]
672260233 => [moneyline:[home:3.04, away:1.349], spreads:null, status:1]
672258226 => [moneyline:[home:2.81, away:1.4], spreads:null, status:1]
672259142 => [moneyline:[home:2.51, away:1.49], spreads:null, status:1]
672259471 => [moneyline:null, spreads:null, status:1]
672259472 => [moneyline:[home:2.34, away:1.555], spreads:null, status:1]
672254436 => [moneyline:[home:3.22, away:1.317], spreads:null, status:1]
672260512 => [moneyline:[home:3.46, away:1.28], spreads:null, status:1]
672260156 => [moneyline:null, spreads:null, status:1]
672260157 => [moneyline:[home:2.94, away:1.37], spreads:null, status:1]
672258072 => [moneyline:[home:3.22, away:1.317], spreads:[[hdp:1.5, home:1.775, away:1.97]], status:1]
672259204 => [moneyline:null, spreads:null, status:2]
672259205 => [moneyline:[home:2.44, away:1.515], spreads:null, status:2]
672254441 => [moneyline:[home:3.22, away:1.317], spreads:null, status:2]
672260033 => [moneyline:[home:2.41, away:1.526], spreads:null, status:1]
672258515 => [moneyline:[home:3.22, away:1.317], spreads:[[hdp:1.5, home:1.793, away:1.952]], status:1]
672259047 => [moneyline:[home:3.13, away:1.332], spreads:null, status:1]
672259601 => [moneyline:[home:2.26, away:1.595], spreads:null, status:1]
672258280 => [moneyline:[home:2.68, away:1.434], spreads:null, status:1]
672259542 => [moneyline:[home:2.03, away:1.735], spreads:null, status:1]
672259490 => [moneyline:[home:2.07, away:1.704], spreads:null, status:1]
672258305 => [moneyline:[home:3.22, away:1.317], spreads:[[hdp:1.5, home:1.793, away:1.952]], status:1]
672259600 => [moneyline:[home:2.31, away:1.571], spreads:null, status:1]
672258581 => [moneyline:[home:2.85, away:1.39], spreads:null, status:1]
672259934 => [moneyline:[home:2.39, away:1.534], spreads:null, status:1]
672259470 => [moneyline:[home:2.34, away:1.555], spreads:null, status:2]
672258132 => [moneyline:[home:2.67, away:1.44], spreads:null, status:1]
672260223 => [moneyline:[home:3.15, away:1.328], spreads:null, status:1]
672259138 => [moneyline:null, spreads:null, status:1]
672259139 => [moneyline:[home:2.48, away:1.5], spreads:null, status:1]
672260122 => [moneyline:[home:2.41, away:1.526], spreads:null, status:2]
672260544 => [moneyline:[home:3.24, away:1.313], spreads:null, status:1]
672259907 => [moneyline:[home:2.35, away:1.552], spreads:null, status:1]
672258593 => [moneyline:[home:3.22, away:1.317], spreads:[[hdp:1.5, home:1.793, away:1.952]], status:1]
672258594 => [moneyline:[home:2.89, away:1.381], spreads:null, status:1]
672258976 => [moneyline:[home:3.13, away:1.332], spreads:null, status:1]
672259667 => [moneyline:[home:2.42, away:1.523], spreads:null, status:1]
672259338 => [moneyline:[home:2.43, away:1.518], spreads:null, status:1]
672260399 => [moneyline:[home:4.03, away:1.217], spreads:null, status:1]
672258715 => [moneyline:[home:3.08, away:1.341], spreads:null, status:1]
672259793 => [moneyline:[home:2.4, away:1.531], spreads:null, status:1]
672260285 => [moneyline:[home:4.11, away:1.21], spreads:null, status:1]
672257848 => [moneyline:[home:3.22, away:1.317], spreads:[[hdp:1.5, home:1.781, away:1.97]], status:1]
672259395 => [moneyline:null, spreads:null, status:2]
672259396 => [moneyline:[home:2.65, away:1.444], spreads:null, status:2]
672254459 => [moneyline:null, spreads:null, status:2]
672256264 => [moneyline:[home:3.22, away:1.317], spreads:null, status:2]
672259273 => [moneyline:[home:2.2, away:1.625], spreads:null, status:2]
672257918 => [moneyline:[home:3.22, away:1.317], spreads:[[hdp:1.5, home:1.719, away:2.05]], status:1]
672259778 => [moneyline:[home:2.32, away:1.564], spreads:null, status:1]
672260297 => [moneyline:[home:4.02, away:1.217], spreads:null, status:1]
672258803 => [moneyline:[home:3.22, away:1.317], spreads:[[hdp:1.5, home:1.793, away:1.952]], status:1]
672258820 => [moneyline:[home:3.04, away:1.349], spreads:null, status:1]
672260418 => [moneyline:[home:3.67, away:1.254], spreads:null, status:1]
672256372 => [moneyline:[home:2.64, away:1.448], spreads:null, status:1]
672259319 => [moneyline:[home:2.38, away:1.54], spreads:null, status:1]
672259638 => [moneyline:[home:2.53, away:1.483], spreads:null, status:1]
672259019 => [moneyline:null, spreads:null, status:1]
672259871 => [moneyline:[home:2.5, away:1.495], spreads:null, status:1]
```

You can see that there are messages with `moneyline` and/or `spreads` `null` but the `lineId`s are all unique in this set.
