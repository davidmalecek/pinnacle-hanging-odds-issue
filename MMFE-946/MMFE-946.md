# Fetched Data vs. Emitted Odds

## Fetched Data
- Event (`eventId`)
  - periods (`[]`)
    - each period contains:
      - moneyline
      - spreads
    - period can be MatchMoneyLine or SeriesMoneyLine

Example: [data/fetched-data.json](data/fetched-data.json)

eventId: `959723927` => 2 periods => 3 markets:
- SeriesMoneyLine (`{"home": 3.22, "away": 1.317}`)
- Spread Handicap 1.5 (`{"hdp": 1.5, "home": 1.793, "away": 1.952}`)
- MatchMoneyLine (`{"home": 3.04, "away": 1.349}`)

## Emitted Odds
The fetched data above are converted into 2 events and 3 markets in [data/emitted-odds.json](data/emitted-odds.json):

```
"markets": [{
  "market_name": "MatchMoneyLine",
  "market_state": "open",
  "odds": [{
      "selection_name": "home",
      "selection_order": 1,
      "value": 3.04
    },
    {
      "selection_name": "away",
      "selection_order": 2,
      "value": 1.349
    }
  ]
}]
```
and
```
"markets": [{
    "market_name": "SeriesMoneyLineTwoWay",
    "market_state": "open",
    "odds": [{
        "selection_name": "home",
        "selection_order": 1,
        "value": 3.22
      },
      {
        "selection_name": "away",
        "selection_order": 2,
        "value": 1.317
      }
    ]
  },
  {
    "market_name": "SeriesHandicapAway1.5",
    "market_state": "open",
    "odds": [{
        "selection_name": "home",
        "selection_order": 1,
        "value": 1.793
      },
      {
        "selection_name": "away",
        "selection_order": 2,
        "value": 1.952
      }
    ]
  }
]
```
