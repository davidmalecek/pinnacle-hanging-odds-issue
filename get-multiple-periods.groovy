import groovy.json.*
import java.io.File
import java.nio.file.Paths
import java.nio.file.Files

if (this.args.length < 1) {
  println "usage: groovy parse-pinnacle-odds.groovy [requiredEventId]"
  return 1
}

def requiredEventId = Integer.valueOf(this.args[0])

def dir = Paths.get('./data')
def jsonSlurper = new JsonSlurper()

def resultMap = [:]

dir.toFile().eachFileMatch(~/xx.*.json/) { file ->
  def json = jsonSlurper.parseText(file.text)

  // odds message
  if (json.containsKey('leagues')) {

    def leagues = json.leagues
    leagues.each { league ->
      def events = league.events
      events.each { event ->
        if (event.id == requiredEventId) {
          def periods = event.periods
          if (periods.size() > 1) {
            println "periods with size ${periods.size()}"
            periods.each { period ->
              println "lineId: $period.lineId => [moneyline: $period.moneyline, spreads: $period.spreads, status: $period.status]"
            }
            // resultMap.put(period.lineId, [moneyline: period.moneyline, spreads: period.spreads, status: period.status])
          }
        }
      }
    }
  }
}

resultMap.each { lineId, period ->
  println "$lineId => $period"
}
